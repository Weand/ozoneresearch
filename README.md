# ozoneresearch

## general information

-> see notes first for cartopy error

Project to investigate the ozonelayer and its changing conditions over time.


The project can be installed in your environment (preferably virtual environment) using `pip`.
It provides the following shell script: `ozoneresearch`.
For usage information, type `ozoneresearch --help`.
your command should look like this: ozoneresearch -c plot_vert -f ozonefile -start 1991-01-01 -stop 1996-01-01
The project has 3 options to calculate and plot, the plot can then be saved in your prefered location
using the save button in the bottom of the plot.

The Input file has to be netCDF file type. 
The file needs to have the foolowing coordinates:

Coordinates:
  * altitude        (altitude) float64 
  * time            (time) datetime64[ns] 
  * latitude_bins   (latitude_bins) float64 -85.0 ... 85.0
  * longitude_bins  (longitude_bins) float64 -175.0 ... 175.0



##  scientific information

general information about the ozone layer in the atmosphere:
https://www.epa.gov/ozone-layer-protection/basic-ozone-layer-science

some information about the evolution of the antartic ozone hole:
https://earthobservatory.nasa.gov/world-of-change/Ozone

## Installation

Use the following command in the base directory to install:

```bash
python -m pip install .
```

For an editable ("developer mode") installation, use the following
instead:

```bash
python -m pip install -e .
```



## Prerequisites

You need a working Python environment, and `pip` installed.

E.g., with `conda`:

```bash
conda create --name mynewenv python
conda activate mynewenv
python -m pip install -e .
```


## Notes
if you get an cartopy and or geos and or shapely error when installing the project

1. Install the C-library beforehand: If you are on Linux (Debian-based, such as Ubuntu or Mint),
just enter `sudo apt install libgeos-dev`.
After that, installation with pip should work. In addition, you might need to do the following though:

pip uninstall shapely
pip install shapely --no-binary shapely

This is also related to geos, and the shapely library, which is also needed by cartopy.

2. If you use mamba/conda, install cartopy manually with mamba in your virtual environment before installing the project.
 
