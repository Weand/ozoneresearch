import argparse
import xarray as xr
import os
import matplotlib.pyplot as plt
from . import fileIO
from . import calcmodule
from . import plotmodule


def ozoneresearch():
    """Entry point for the ozone project"""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--calculate",
        "-c",
        required=True,
        help="Choose Options to:"
        "calculate vertical profiles of ozone distribution for different areas (=plot_vert),"
        "Height of the maximum ozone level for all season (=global_height), "
        "trend of the ozone concentration in NH Spring to investigate the ozone hole (=ozone_hole)."
        "To save the plot use the Save Button in the bottom of the plot",
    )

    parser.add_argument(
        "--filename",
        "-fn",
        required=True,
        help="Select a netCDF-file eg. 'path/filename'.",
    )

    parser.add_argument(
        "--start",
        "-start",
        required=True,
        help="write startdate",
    )

    parser.add_argument(
        "--stop",
        "-stop",
        required=True,
        help="write stopdate",
    )

    args = parser.parse_args()
    xr.set_options(keep_attrs=True)
    ds_raw = fileIO.netCDF_IO(args.filename)
    ds_rawcut = calcmodule.selectperiod(ds_raw, args.start, args.stop)

    if args.calculate == "plot_vert":

        ds_verto3 = calcmodule.calcverto3(ds_rawcut)

        verto3plot = plotmodule.plotverto3(ds_verto3, args.start, args.stop)
        print("hektor")

    if args.calculate == "plot_globalheight":

        height_max_ozone_seasonal = calcmodule.altitude_max_ozone_seasonal(ds_rawcut)
        plotmodule.plot_altitude_max_ozone_seasonal(
            height_max_ozone_seasonal,
        )

    if args.calculate == "plot_holetrend":

        df_o3hole = calcmodule.calc_o3hole(ds_rawcut)
        plotmodule.plotholetrend(df_o3hole)
