import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

xr.set_options(keep_attrs=True)


def calcverto3(ds_rawcut):
    """
    takes input array, slices to regions, calculates long lat time mean and gives array with all data to plot
    """
    worldwide = slice(-85, 85)
    tropics = slice(-10, 10)
    midlat = slice(50, 60)
    highlat = slice(60, 85)
    antarctic = slice(-85, -70)
    # mean over selectes latitude , whole time and whole longitude,
    dsww = ds_rawcut.ozone.sel(latitude_bins=(worldwide))
    dsww = dsww.mean("latitude_bins").mean("time").mean("longitude_bins")
    dstrop = ds_rawcut.ozone.sel(latitude_bins=(tropics))
    dstrop = dstrop.mean("latitude_bins").mean("time").mean("longitude_bins")
    dsmidlat = ds_rawcut.ozone.sel(latitude_bins=(midlat))
    dsmidlat = dsmidlat.mean("latitude_bins").mean("time").mean("longitude_bins")
    dshighlat = ds_rawcut.ozone.sel(latitude_bins=(highlat))
    dshighlat = dshighlat.mean("latitude_bins").mean("time").mean("longitude_bins")
    dsantarctic = ds_rawcut.ozone.sel(latitude_bins=(antarctic))
    dsantarctic = dsantarctic.mean("latitude_bins").mean("time").mean("longitude_bins")
    # name variables
    dsvertall = xr.Dataset(
        {
            "ozoneww": dsww,
            "ozonetrop": dstrop,
            "ozonemidlat": dsmidlat,
            "ozonehighlat": dshighlat,
            "ozoneantarctic": dsantarctic,
        }
    )
    return dsvertall


def selectperiod(ds_raw, start, stop):
    """
    Selects a Period and gives back datarray, format 1991-01-01

    """

    ds_rawcut = ds_raw.sel(time=slice(start, stop))
    return ds_rawcut


def calc_o3hole(ds_rawcut):
    """
    takes raw input file(dataarray) and gives dataframe with mean ozone conc. of antartic stratosphere NH Spring
    """
    ds_o3holestrat = slicehole(ds_rawcut, 10000, 60000)
    ds_o3hole1020 = slicehole(ds_rawcut, 10000, 20000)
    ds_o3hole2030 = slicehole(ds_rawcut, 20000, 30000)
    ds_o3hole3040 = slicehole(ds_rawcut, 30000, 40000)
    ds_o3hole4050 = slicehole(ds_rawcut, 40000, 50000)
    ds_o3hole5060 = slicehole(ds_rawcut, 50000, 60000)

    df_o3holestrat = holedstodf(ds_o3holestrat)
    df_o3hole1020 = holedstodf(ds_o3hole1020)
    df_o3hole2030 = holedstodf(ds_o3hole2030)
    df_o3hole3040 = holedstodf(ds_o3hole3040)
    df_o3hole4050 = holedstodf(ds_o3hole4050)
    df_o3hole5060 = holedstodf(ds_o3hole5060)

    df_o3holeall = pd.concat(
        [
            df_o3holestrat,
            df_o3hole1020,
            df_o3hole2030,
            df_o3hole3040,
            df_o3hole4050,
            df_o3hole5060,
        ],
        axis=1,
    )

    # you need the following 4 lines to rename duplicate column name
    cols = pd.Series(df_o3holeall.columns)
    for dup in cols[cols.duplicated()].unique():
        cols[cols[cols == dup].index.values.tolist()] = [
            dup + "." + str(i) if i != 0 else dup for i in range(sum(cols == dup))
        ]
    # rename the columns with the cols list.
    df_o3holeall.columns = cols

    return df_o3holeall


def slicehole(ds_o3hole, lowerlimit, upperlimit):
    """
    slices array to given height between lowerlimit and upperlimit, makes mean of long lat and altitude then
    """

    ds_o3hole = ds_o3hole.sel(latitude_bins=(slice(-85, -70)))
    ds_o3hole = ds_o3hole.sel(altitude=(slice(lowerlimit, upperlimit)))
    ds_o3holeslicemean = (
        ds_o3hole.mean("latitude_bins").mean("altitude").mean("longitude_bins")
    )
    return ds_o3holeslicemean


def holedstodf(ds_o3holeregion):
    """
    changes array to frame, column name year and month, slices to NH spring and means then, gives frame !split this in 2 function!

    """

    df_o3 = ds_o3holeregion.ozone.reset_coords(drop=True).to_dataframe()
    df_o3["month"] = df_o3.index.month
    df_o3["year"] = df_o3.index.year

    df_o3month = df_o3.loc[df_o3["month"].isin([8, 9, 10])]
    df_o3month = df_o3month.groupby("year").mean()
    df_o3month = df_o3month.ozone
    df_o3month = df_o3month.to_frame()

    return df_o3month


def altitude_max_ozone(ds_o3timean):
    """
    calculates height of the maximum ozone level

    """
    condition_max_ozone = ds_o3timean == ds_o3timean.max(dim="altitude")
    height_max_ozone_mask = xr.where(
        condition_max_ozone, ds_o3timean["altitude"], np.nan
    )
    height_max_ozone = height_max_ozone_mask.to_dataset().drop("altitude")
    height_max_ozone = height_max_ozone_mask.mean("altitude")
    return height_max_ozone


def altitude_max_ozone_seasonal(ds_input):
    """
    Calculates specific height of max ozone from all seasons, give array

    """
    ds_ozone_timean_seasons = ds_input.ozone.groupby("time.season").mean()

    for season in ds_ozone_timean_seasons.season:
        ds_ozone_timean_seasons.sel(season=season)
        height_max_ozone = altitude_max_ozone(ds_ozone_timean_seasons)
        height_max_ozone_transposed = height_max_ozone.transpose(
            "season", "latitude_bins", "longitude_bins", transpose_coords=True
        )
    return height_max_ozone_transposed
