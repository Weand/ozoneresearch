import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs


def plotverto3(ds_verto3, startdate, enddate):
    """
    Plot vertical ozone profile for different areas

    """
    x1 = ds_verto3["ozoneww"]
    x2 = ds_verto3["ozonetrop"]
    x3 = ds_verto3["ozonemidlat"]
    x4 = ds_verto3["ozonehighlat"]
    x5 = ds_verto3["ozoneantarctic"]
    y = ds_verto3["altitude"]
    title = (
        "vertical profiles of mean ozone concentration"
        + " "
        + startdate
        + " - "
        + enddate
    )
    fig = plt.figure(figsize=(8, 10))
    plt.plot(x1, y, color="k", label="Global", linewidth=2)
    plt.plot(x2, y, color="g", label="Tropics", linewidth=1)
    plt.plot(x3, y, color="m", label="Mid Latitudes", linewidth=1)
    plt.plot(x4, y, color="y", label="High Latitudes", linewidth=1)
    plt.plot(x5, y, color="c", label="Antarctic", linewidth=1)
    plt.xlabel("Ozone Concentration")
    plt.ylabel("Altitude [m]")
    plt.title(title)

    plt.legend()
    plt.grid(color="black", linestyle="--", linewidth=0.2)

    return plt.show()


def plot_altitude_max_ozone_seasonal(ds_processed):
    """
    plots global maps of the max ozone height for each season

    """
    fig, axes = plt.subplots(
        nrows=4, subplot_kw={"projection": ccrs.EqualEarth()}, figsize=(7, 11)
    )

    for i, season in enumerate(("DJF", "MAM", "JJA", "SON")):
        ozone_plot = ds_processed.sel(season=season).plot.pcolormesh(
            ax=axes[i],
            transform=ccrs.PlateCarree(),
            vmin=30000,
            vmax=38000,
            add_colorbar=False,
            extend="both",
        )

    for n, ax in enumerate(axes):
        ax.coastlines()
        gl = ax.gridlines(draw_labels=True)
        gl.top_labels = False

    cax = plt.axes([0.85, 0.05, 0.04, 0.85])
    fig.colorbar(ozone_plot, cax=cax, ax=axes[:], shrink=0.8)
    plt.suptitle("Height of maximum ozone level", x=0.55, y=0.95, fontsize=16)
    plt.subplots_adjust(
        left=0.1, bottom=0.05, right=0.8, top=0.9, wspace=0.4, hspace=0.4
    )
    return plt.show()


def plotholetrend(df_input):
    """
    Plot trend of the ozone concentration in antarcrtic

    """
    df_o3hole = df_input
    x = df_o3hole.index
    y1 = df_o3hole[df_o3hole.columns[0]]
    y2 = df_o3hole[df_o3hole.columns[1]]
    y3 = df_o3hole[df_o3hole.columns[2]]
    y4 = df_o3hole[df_o3hole.columns[3]]
    y5 = df_o3hole[df_o3hole.columns[4]]
    y6 = df_o3hole[df_o3hole.columns[5]]

    fig = plt.figure(figsize=(8, 10))
    plt.plot(x, y1, color="k", label="8km-60km", linestyle="--", linewidth=2)
    plt.plot(x, y2, color="b", label="10km-20km", linestyle="--", linewidth=1)
    plt.plot(x, y3, color="g", label="20km-30km", linewidth=1)
    plt.plot(x, y4, color="c", label="30km-40km", linewidth=1)
    plt.plot(x, y5, color="r", label="40km-50km", linestyle="--", linewidth=1)
    plt.plot(x, y6, color="m", label="50km-60km", linestyle="--", linewidth=1)

    plt.xlabel("Year")
    plt.ylabel("Ozone Concentration")
    plt.title(
        "Ozone Concentration in the Antarctic Atmosphere 85°S - 75°S in NH Spring",
        fontsize=10,
        y=1.02,
    )
    plt.suptitle(
        "Relevant heights for ozone hole = continuous lines", fontsize=8, y=0.9
    )
    plt.grid(color="black", linestyle="--", linewidth=0.2)
    plt.legend()
    plt.xticks(np.arange(min(x), max(x) + 1, 1.0), rotation="vertical")

    plt.show()
