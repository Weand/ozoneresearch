import xarray as xr


def netCDF_IO(filename):
    """
    Reads data from file, assuming netCDF4 format

    Args:
        filename: Filename of file containing the data.

    Returns: xr dataset with all data
    """
    with xr.open_dataset(filename) as ds_input:
        ds_input.load()
    return ds_input
